var gulp = require('gulp');
// var uglify = require('gulp-uglify');
var compass = require('gulp-compass');
var imagemin = require('gulp-imagemin');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();


gulp.task('autoprefix', () =>
    gulp.src('css/main.css')
        .pipe(autoprefixer({
            browsers: ['> 5%'],
            remove:false,
            cascade: false
        }))
        .pipe(gulp.dest('css'))
);

gulp.task('compass', function() {
    gulp.src('sass/**/*.scss')
        .pipe(compass({
            config_file: 'config.rb',
            css: 'css',
            sass: 'sass'
        }))
        .pipe(gulp.dest('css'))
});

// gulp.task('minify-css', function() {
//     return gulp.src('css/**/*.css')
//         .pipe(cleanCSS({ compatibility: 'ie8' }))
//         .pipe(gulp.dest('css'));
// });

// gulp.task('imageminify', () =>

//     gulp.src('images/*')
//     .pipe(imagemin())
//     .pipe(gulp.dest('images'))

// );

gulp.task('browserSync',function(){
  browserSync.init({
    proxy:"drupal-20/"
  })
});

gulp.task('default', ['compass','browserSync'], function() {
    gulp.watch('sass/**/*.scss', ['compass']);
    gulp.watch('images/**/*', ['imageminify']);
    // gulp.watch('css/**/*.css', ['minify-css']);
    gulp.watch("./", ['browserSync']);
    // gulp.watch('js/**/*.js', ['scripts']);
});

// gulp.task('scripts', function() {
//     gulp.src('themes/internship2017/js/**/*.js')
//         .pipe(uglify())
//         .pipe(gulp.dest('themes/internship2017/js'))
// });

// gulp.task('jslint',function(){
//  return gulp.src('js/**/.js')
//     .pipe(jshint())
//     .pipe(jshint.reporter('default'));
// });
// 
